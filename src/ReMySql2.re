let unexpectedResultError = (msg, code, sql) =>
  Relude_Js_Json.(
    [|
      ("name", "IncompatibleResult" |> fromString),
      ("msg", msg |> fromString),
      ("code", code |> fromString),
      ("errno", 99999 |> fromInt),
      ("sql", sql |> fromString),
    |]
    |> Relude_Js_Json.fromArrayOfKeyValueTuples
  )
  |> MySql2.Exn.fromJs;

module Connection = MySql2.Connection;

module Exn = {
  include MySql2.Exn;

  module Internal = {
    [@bs.deriving abstract]
    type t = {
      name: option(string),
      msg: option(string),
      code: option(string),
      errno: option(int),
      sql: option(string),
      sqlState: option(string),
      sqlMessage: option(string),
    };

    external unsafeConvert: MySql2.Exn.t => t = "%identity";
  };

  let nameGet = t =>
    Internal.unsafeConvert(t)
    |> Internal.nameGet
    |> Relude_Option.getOrElse("UNKNOWN");

  let codeGet = t =>
    Internal.unsafeConvert(t)
    |> Internal.codeGet
    |> Relude_Option.getOrElse("EMPTY_MESSAGE");

  let errnoGet = t =>
    Internal.unsafeConvert(t)
    |> Internal.errnoGet
    |> Relude_Option.getOrElse(99999);

  let msgGet = t =>
    Internal.unsafeConvert(t)
    |> Internal.msgGet
    |> Relude_Option.getOrElse("Empty Message");

  let sqlGet = t => Internal.unsafeConvert(t) |> Internal.sqlGet;

  let sqlStateGet = t => Internal.unsafeConvert(t) |> Internal.sqlStateGet;

  let sqlMessage = t => Internal.unsafeConvert(t) |> Internal.sqlMessageGet;
};

module Meta = MySql2.Meta;

module Mutation = MySql2.Mutation;

module Params = MySql2.Params;

module Select = MySql2.Select;

let mutate = (db, sql, params) =>
  Relude_IO.async(onDone =>
    MySql2.execute(
      db,
      sql,
      params,
      fun
      | `Error(e) => onDone(e |> Relude_Result.error)
      | `Select(_) =>
        onDone(
          unexpectedResultError(
            "Expected a mutation result, but received a select query result.",
            "UNEXPECTED_SELECT_RESULT",
            sql,
          )
          |> Relude_Result.error,
        )
      | `Mutation(m) => onDone(m |> Relude_Result.ok),
    )
  );

let select = (db, sql, params) =>
  Relude_IO.async(onDone =>
    MySql2.execute(
      db,
      sql,
      params,
      fun
      | `Error(e) => onDone(e |> Relude_Result.error)
      | `Mutation(_) =>
        onDone(
          unexpectedResultError(
            "Expected a select query result, but received a mutation result.",
            "UNEXPECTED_MUTATION_RESULT",
            sql,
          )
          |> Relude_Result.error,
        )
      | `Select(s) => onDone(s |> Relude_Result.ok),
    )
  );
