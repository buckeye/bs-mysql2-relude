open Relude.Globals;

let connect = () =>
  ReMySql2.Connection.connect(
    ~host="127.0.0.1",
    ~port=3306,
    ~user="root",
    ~password="",
    ~database="test",
    (),
  );

let table_sql = {|
  CREATE TABLE IF NOT EXISTS `test`.`mysql2` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT
  , `code` varchar(32) NOT NULL
  , `display` varchar(140) DEFAULT NULL
  , PRIMARY KEY(`id`)
  )
|};

let seed_sql = {|
  INSERT IGNORE INTO `test`.`mysql2`
  (`id`, `code`, `display`)
  VALUES
  (1, "batman", "I am, Bat-man"),
  (2, "superman", "Truth, Justice and the American Way"),
  (3, "wonder_woman", "Suffering Sappho!")
|};

module Row = {
  [@bs.deriving abstract]
  type t = {
    id: string,
    code: string,
    display: string,
  };

  let make = (id, code, display) => t(~id, ~code, ~display);

  external unsafeDecode: Js.Json.t => t = "%identity";
};

let db: ref(option(ReMySql2.Connection.t)) = ref(None);

let getDb = () =>
  switch (db^) {
  | Some(connection) => connection
  | None => failwith("Could not retrieve a database connection")
  };

Ava.Before.promiseAll(
  Ava.ava,
  ~message="Load Database Connection",
  _ => {
    db := Some(connect());

    ReMySql2.mutate(getDb(), "DROP TABLE IF EXISTS `test`.`mysql2`", None)
    |> IO.flatMap(_ => ReMySql2.mutate(getDb(), table_sql, None))
    |> IO.flatMap(_ => ReMySql2.mutate(getDb(), seed_sql, None))
    |> Relude_Js_Promise.fromIO;
  },
);

Ava.Async.test(Ava.ava, "Should discover the Bat-maan", t =>
  ReMySql2.select(getDb(), "SELECT * FROM `test`.`mysql2`", None)
  |> IO.map(r => ReMySql2.Select.flatMap(r, Row.unsafeDecode))
  |> IO.unsafeRunAsync(result =>
       result
       |> Result.map(Array.any(row => row->Row.codeGet == "batman"))
       |> Result.tapOk(result => {
            Ava.Assert.true_(t, result);
            Ava.Test.finish(t);
          })
       |> Result.tapError(e => {
            Js.Console.log2("Failed to find Bat-man", e);
            Ava.Test.fail(t);
          })
       |> ignore
     )
);

Ava.Async.test(
  Ava.ava, "Should pass through a MySql error for a select call", t =>
  ReMySql2.select(getDb(), "SELECT * FROM `test`.`does_not_exist`", None)
  |> IO.unsafeRunAsync(result =>
       result
       |> Result.tapOk(success => {
            Js.Console.log2("Unexpected result: ", success);
            Ava.Test.fail(t);
            Ava.Test.finish(t);
          })
       |> Result.tapError(e => {
            Ava.Assert.is(t, "ER_NO_SUCH_TABLE", e->ReMySql2.Exn.codeGet);
            Ava.Test.finish(t);
          })
       |> ignore
     )
);

Ava.Async.test(
  Ava.ava, "Should pass through a MySql error for a mutate call", t =>
  ReMySql2.mutate(getDb(), "SELECT * FROM `test`.`does_not_exist`", None)
  |> IO.unsafeRunAsync(result =>
       result
       |> Result.tapOk(success => {
            Js.Console.log2("Unexpected result: ", success);
            Ava.Test.fail(t);
            Ava.Test.finish(t);
          })
       |> Result.tapError(e => {
            Ava.Assert.is(t, "ER_NO_SUCH_TABLE", e->ReMySql2.Exn.codeGet);
            Ava.Test.finish(t);
          })
       |> ignore
     )
);

Ava.Async.test(
  Ava.ava, "Should error when passing a SELECT query to mutate", t =>
  ReMySql2.mutate(getDb(), "SELECT * FROM `test`.`mysql2`", None)
  |> IO.unsafeRunAsync(result =>
       result
       |> Result.tapOk(success => {
            Js.Console.log2("Unexpected result: ", success);
            Ava.Test.fail(t);
            Ava.Test.finish(t);
          })
       |> Result.tapError(e => {
            Ava.Assert.is(
              t,
              "UNEXPECTED_SELECT_RESULT",
              e->ReMySql2.Exn.codeGet,
            );
            Ava.Assert.is(t, "IncompatibleResult", e->ReMySql2.Exn.nameGet);
            Ava.Assert.is(
              t,
              "Expected a mutation result, but received a select query result.",
              e->ReMySql2.Exn.msgGet,
            );
            Ava.Assert.is(t, 99999, e->ReMySql2.Exn.errnoGet);
            Ava.Assert.is(
              t,
              "SELECT * FROM `test`.`mysql2`",
              e->ReMySql2.Exn.sqlGet |> Option.getOrElse("EMPTY"),
            );
            Ava.Assert.true_(t, e->ReMySql2.Exn.sqlStateGet->Option.isNone);
            Ava.Assert.true_(t, e->ReMySql2.Exn.sqlMessage->Option.isNone);
            Ava.Test.finish(t);
          })
       |> ignore
     )
);

Ava.Async.test(
  Ava.ava,
  "Should error when passing an INSERT query to select",
  t => {
    let sql = {|
    INSERT INTO `test`.`mysql2`
    (`id`, `code`, `display`)
    VALUES
    (4, "flash", "I am the fastest man alive")
  |};

    ReMySql2.select(getDb(), sql, None)
    |> IO.unsafeRunAsync(result =>
         result
         |> Result.tapOk(success => {
              Js.Console.log2("Unexpected result: ", success);
              Ava.Test.fail(t);
              Ava.Test.finish(t);
            })
         |> Result.tapError(e => {
              Ava.Assert.is(
                t,
                "UNEXPECTED_MUTATION_RESULT",
                e->ReMySql2.Exn.codeGet,
              );
              Ava.Assert.is(t, "IncompatibleResult", e->ReMySql2.Exn.nameGet);
              Ava.Assert.is(
                t,
                "Expected a select query result, but received a mutation result.",
                e->ReMySql2.Exn.msgGet,
              );
              Ava.Assert.is(t, 99999, e->ReMySql2.Exn.errnoGet);
              Ava.Assert.true_(t, e->ReMySql2.Exn.sqlGet->Option.isSome);
              Ava.Assert.true_(t, e->ReMySql2.Exn.sqlStateGet->Option.isNone);
              Ava.Assert.true_(t, e->ReMySql2.Exn.sqlMessage->Option.isNone);
              Ava.Test.finish(t);
            })
         |> ignore
       );
  },
);
